﻿using System.Collections.Generic;

namespace AgentAPI.Models
{
    public class ResponseObjects
    {
        public class Rates
        {
            public List<Rate> rates { get; set; }
            
            public class Rate
            {
                public string sourceCurrency { get; set; }
                public string destinationCurrency { get; set; }
                public string destinationCountry { get; set; }
                public List<Delivery> deliveries { get; set; }
                public List<Payment> payments { get; set; }
                public List<string> times { get; set; }
                
                public class Delivery
                {
                    public string method { get; set; }
                    public decimal minimum { get; set; }
                    public decimal maximum { get; set; }
                    public List<int> amounts { get; set; }
                    public decimal rate { get; set; }
                }

                public class Payment
                {
                    public string method { get; set; }
                    public decimal minimum { get; set; }
                    public decimal maximum { get; set; }
                    public List<Fee> fees { get; set; }
                    

                    public class Fee
                    {
                        public decimal amount { get; set; }
                        public decimal lowerBound { get; set; }
                        public decimal upperBound { get; set; }
                        public string deliveryTime { get; set; }
                    }
                }
            }
        }

        public class Quote
        {
            public decimal rate { get; set; }
            public decimal fee { get; set; }
            public decimal destinationAmount { get; set; }
            public decimal sourceAmount { get; set; }
            public decimal tax { get; set; }
            public decimal amountToPay { get; set; }
            public string quoteReference { get; set; }
            public string message { get; set; }
        }

        public class Transaction
        {
            public string transactionReference { get; set; }
            public string status { get; set; }
            public string message { get; set; }
        }

        public class Initiate
        {
            public string quoteReference { get; set; }
            public string status { get; set; }
            public string message { get; set; }
        }

        public class Banks
        {
            public List<Bank> banks { get; set; }
            
            public class Bank
            {
                public string code { get; set; }
                public string name { get; set; }
                public string country { get; set; }
                public List<Branch> branches { get; set; }

                public class Branch
                {
                    public string code { get; set; }
                    public string name { get; set; }
                }
            }
        }

        public class Payment
        {
            public string businessShortCode { get; set; }
            public string transactionTime { get; set; }
            public decimal amount { get; set; }
            public string payBillReferenceNumber { get; set; }
            public string name { get; set; }
            public string mobile { get; set; }
            public string paymentReference { get; set; }
            public string transactionReference { get; set; }
            public string error { get; set; }
            public string status { get; set; }
        }

        public class UssdRecipients
        {
            public List<Recipient> recipients { get; set; }

            public class Recipient
            {
                public string mobile { get; set; }
                public string name { get; set; }
            }
        }

        public class UssdTransactionInsert
        {
            public string message { get; set; }
            public string insertToken { get; set; }
            public decimal destAmount { get; set; }
            public string destCurrency { get; set; }
            public decimal sourceAmount { get; set; }
            public string sourceCurrency { get; set; }
            public decimal fee { get; set; }
            public decimal rate { get; set; }
            public string recipientMobile { get; set; }
            public string recipientName { get; set; }
        }

        public class UssdTransactionConfirm
        {
            public string status { get; set; }
            public string transactionReference { get; set; }
            public string message { get; set; }
        }

        public class VerifyPhone
        {
            public string status { get; set; }
            public string E164 { get; set; }
            public string country { get; set; }
            public string message { get; set; }
        }
    }
}