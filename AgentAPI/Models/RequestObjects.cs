﻿namespace AgentAPI.Models
{
    public class RequestObjects
    {
        public class GetRates
        {
            public string sourceCountry { get; set; }
            public string destinationCountry { get; set; }
        }

        public class GetBanks
        {
            public string country { get; set; }

        }
        public class GetQuotes
        {
            public string sourceCountry { get; set; }
            public string destinationCountry { get; set; }
            public string destinationCurrency { get; set; }
            public string deliveryMethod { get; set; }
            public string deliveryTime { get; set; }
            public string paymentMethod { get; set; }
            public decimal amount { get; set; }
        }

        public class CreateTrans
        {
            public Transaction transaction { get; set; }
            public int version { get; set; }

            public class Transaction
            {
                public Sender sender { get; set; }
                public Recipient recipient { get; set; }
                public string quoteReference { get; set; }
                public string agentReference { get; set; }
                public string callBackUrl { get; set; }

                public class Sender
                {
                    public string firstName { get; set; }
                    public string lastName { get; set; }
                    public string mobile { get; set; }
                    public Address address { get; set; }
                    public string dateOfBirth { get; set; }
                    public string email { get; set; }
                    public string nationality { get; set; }
                    public ID id { get; set; }

                    public class ID
                    {
                        public string type { get; set; }
                        public string number { get; set; }
                        public string issuedBy { get; set; }
                        public string startDate { get; set; }
                        public string expiryDate { get; set; }
                    }
                }

                public class Recipient
                {
                    public string firstName { get; set; }
                    public string lastName { get; set; }
                    public string mobile { get; set; }
                    public Address address { get; set; }
                    public string email { get; set; }
                    public Bank bankDetails { get; set; }

                    public class Bank
                    {
                        public string accountNumber { get; set; }
                        public string bankCode { get; set; }
                        public string branchCode { get; set; }
                        public string country { get; set; }
                        public string iban { get; set; }
                        public string swiftCode { get; set; }
                        public string bsb { get; set; }
                    }
                }

                public class Address
                {
                    public string addressLine1 { get; set; }
                    public string addressLine2 { get; set; }
                    public string city { get; set; }
                    public string state { get; set; }
                    public string postcode { get; set; }
                    public string country { get; set; }
                }
            }
        }

        public class GetTrans
        {
            public string transactionReference { get; set; }
            public string agentReference { get; set; }
        }

        public class CommitTrans
        {
            public string quoteReference { get; set; }
        }

        public class VerifyPhone
        {
            public string mobile { get; set; }
            public string country { get; set; }
        }

        public class GetPayment
        {
            public string reference { get; set; }
        }

        public class USSDGetRecs
        {
            public string mobile { get; set; }
        }

        public class USSDInsertTrans
        {
            public string mobile { get; set; }
            public decimal amount { get; set; }
            public string recipientMobile { get; set; }
            public string recipientName { get; set; }
            public string businessNumber { get; set; }
            public string senderAddress { get; set; }
            public string senderCity { get; set; }
            public string senderPostCode { get; set; }
            public string senderIDNumber { get; set; }
            public string senderIDExpiry { get; set; }
            public string senderIDType { get; set; }
            public string senderName { get; set; }
            public string senderDateOfBirth { get; set; }
        }

        public class USSDConfirmTrans
        {
            public string insertToken { get; set; }
            public string transactionID { get; set; }
        }
    }
}