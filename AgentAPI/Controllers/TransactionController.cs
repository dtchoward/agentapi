﻿using AgentAPI.Models;
using AgentAPI.Repositories;
using Newtonsoft.Json;
using System;
using System.Web.Mvc;
using static AgentAPI.Models.ResponseObjects;

namespace AgentAPI.Controllers
{
    public class TransactionController : Controller
    {
        #region Quote
        [AllowAnonymous]
        public JsonResult Quote(RequestObjects.GetQuotes p)
        {
            try
            {
                string res = ForwardDAL.PostToAPI(this.Request, "getquote", JsonConvert.SerializeObject(p));

                try
                {
                    return Json(JsonConvert.DeserializeObject<Quote>(res), JsonRequestBehavior.DenyGet);
                }
                catch
                {
                    return Json(res, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }
        #endregion

        #region Create
        [AllowAnonymous]
        public JsonResult Create(RequestObjects.CreateTrans p)
        {
            try
            {
                p.version = 0;
                string res = ForwardDAL.PostToAPI(this.Request, "createtransaction", JsonConvert.SerializeObject(p));

                try
                {
                    return Json(JsonConvert.DeserializeObject<Transaction>(res), JsonRequestBehavior.DenyGet);
                }
                catch
                {
                    return Json(res, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }
        #endregion

        #region Get
        [AllowAnonymous]
        public JsonResult Get(RequestObjects.GetTrans p)
        {
            try
            {
                string res = ForwardDAL.PostToAPI(this.Request, "gettransaction", JsonConvert.SerializeObject(p));

                try
                {
                    return Json(JsonConvert.DeserializeObject<Transaction>(res), JsonRequestBehavior.DenyGet);
                }
                catch
                {
                    return Json(res, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }
        #endregion

        #region Initiate
        [AllowAnonymous]
        public JsonResult Initiate(RequestObjects.CreateTrans p)
        {
            try
            {
                p.version = 1;
                string res = ForwardDAL.PostToAPI(this.Request, "createtransaction", JsonConvert.SerializeObject(p));

                try
                {
                    return Json(JsonConvert.DeserializeObject<Initiate>(res), JsonRequestBehavior.DenyGet);
                }
                catch
                {
                    return Json(res, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }
        #endregion

        #region Commit
        [AllowAnonymous]
        public JsonResult Commit(RequestObjects.CommitTrans p)
        {
            try
            {
                string res = ForwardDAL.PostToAPI(this.Request, "committransaction", JsonConvert.SerializeObject(p));

                try
                {
                    return Json(JsonConvert.DeserializeObject<Transaction>(res), JsonRequestBehavior.DenyGet);
                }
                catch
                {
                    return Json(res, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }
        #endregion

        #region Commit
        [AllowAnonymous]
        public JsonResult Phone(RequestObjects.VerifyPhone p)
        {
            try
            {
                string res = ForwardDAL.PostToAPI(this.Request, "verifyphone", JsonConvert.SerializeObject(p));

                try
                {
                    return Json(JsonConvert.DeserializeObject<ResponseObjects.VerifyPhone>(res), JsonRequestBehavior.DenyGet);
                }
                catch
                {
                    return Json(res, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }
        #endregion
    }
}
