﻿using AgentAPI.Models;
using AgentAPI.Repositories;
using Newtonsoft.Json;
using System;
using System.Web.Mvc;
using static AgentAPI.Models.ResponseObjects;

namespace AgentAPI.Controllers
{
    public class RatesController : Controller
    {
        #region Get
        [AllowAnonymous]
        public JsonResult Get(RequestObjects.GetRates p)
        {
            try
            {
                string res = ForwardDAL.PostToAPI(this.Request, "getrates", JsonConvert.SerializeObject(p));

                try
                {
                    return Json(JsonConvert.DeserializeObject<Rates>(res), JsonRequestBehavior.DenyGet);
                }
                catch
                {
                    return Json(res, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }
        #endregion

        [AllowAnonymous]
        public JsonResult Test()
        {
            try
            {
                try
                {
                    return Json("You have connection", JsonRequestBehavior.AllowGet);
                }
                catch
                {
                    return Json("You have connection", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }
    }
}
