﻿using AgentAPI.Models;
using AgentAPI.Repositories;
using Newtonsoft.Json;
using System;
using System.Web.Mvc;
using static AgentAPI.Models.ResponseObjects;

namespace AgentAPI.Controllers
{
    public class PaymentController : Controller
    {
        #region Get
        [AllowAnonymous]
        public JsonResult Get(RequestObjects.GetPayment p)
        {
            try
            {
                string res = ForwardDAL.PostToAPI(this.Request, "getpayment", JsonConvert.SerializeObject(p));

                try
                {
                    return Json(JsonConvert.DeserializeObject<Payment>(res), JsonRequestBehavior.DenyGet);
                }
                catch
                {
                    return Json(res, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }
        #endregion

        #region Paid
        [AllowAnonymous]
        public JsonResult Paid(RequestObjects.GetPayment p)
        {
            try
            {
                string res = ForwardDAL.PostToAPI(this.Request, "paidpayment", JsonConvert.SerializeObject(p));

                try
                {
                    return Json(JsonConvert.DeserializeObject<Payment>(res), JsonRequestBehavior.DenyGet);
                }
                catch
                {
                    return Json(res, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }
        #endregion
    }
}