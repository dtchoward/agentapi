﻿using AgentAPI.Models;
using AgentAPI.Repositories;
using Newtonsoft.Json;
using System;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using static AgentAPI.Models.ResponseObjects;

namespace AgentAPI.Controllers
{
    public class MenuController : Controller
    {
        #region Recipients
        [AllowAnonymous]
        public JsonResult Recipients(RequestObjects.USSDGetRecs p)
        {
            try
            {
                string res = ForwardDAL.PostToAPI(this.Request, "ussdgetrecs", JsonConvert.SerializeObject(p));

                try
                {
                    return Json(JsonConvert.DeserializeObject<UssdRecipients>(res), JsonRequestBehavior.DenyGet);
                }
                catch
                {
                    return Json(res, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }

        [AllowAnonymous]
        public ActionResult Recipients_xml(RequestObjects.USSDGetRecs p)
        {
            string res = ForwardDAL.PostToAPI(this.Request, "ussdgetrecs", JsonConvert.SerializeObject(p));
            var xml = XDocument.Load(JsonReaderWriterFactory.CreateJsonReader(Encoding.ASCII.GetBytes(res), new XmlDictionaryReaderQuotas()));
            
            return this.Content(xml.ToString(), "text/xml", Encoding.UTF8);
        }
        #endregion

        #region Insert
        [AllowAnonymous]
        public JsonResult Insert(RequestObjects.USSDInsertTrans p)
        {
            try
            {
                string res = ForwardDAL.PostToAPI(this.Request, "ussdinserttrans", JsonConvert.SerializeObject(p));

                try
                {
                    return Json(JsonConvert.DeserializeObject<UssdTransactionInsert>(res), JsonRequestBehavior.DenyGet);
                }
                catch
                {
                    return Json(res, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }
        
        [AllowAnonymous]
        public ActionResult Insert_xml(RequestObjects.USSDGetRecs p)
        {
            string res = ForwardDAL.PostToAPI(this.Request, "ussdinserttrans", JsonConvert.SerializeObject(p));
            var xml = XDocument.Load(JsonReaderWriterFactory.CreateJsonReader(Encoding.ASCII.GetBytes(res), new XmlDictionaryReaderQuotas()));

            return this.Content(xml.ToString(), "text/xml", Encoding.UTF8);
        }
        #endregion

        #region Confirm
        [AllowAnonymous]
        public JsonResult Confirm(RequestObjects.USSDConfirmTrans p)
        {
            try
            {
                string res = ForwardDAL.PostToAPI(this.Request, "ussdconfirmtrans", JsonConvert.SerializeObject(p));

                try
                {
                    return Json(JsonConvert.DeserializeObject<UssdTransactionConfirm>(res), JsonRequestBehavior.DenyGet);
                }
                catch
                {
                    return Json(res, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }

        [AllowAnonymous]
        public ActionResult Confirm_xml(RequestObjects.USSDGetRecs p)
        {
            string res = ForwardDAL.PostToAPI(this.Request, "ussdconfirmtrans", JsonConvert.SerializeObject(p));
            var xml = XDocument.Load(JsonReaderWriterFactory.CreateJsonReader(Encoding.ASCII.GetBytes(res), new XmlDictionaryReaderQuotas()));

            return this.Content(xml.ToString(), "text/xml", Encoding.UTF8);
        }
        #endregion
    }
}
