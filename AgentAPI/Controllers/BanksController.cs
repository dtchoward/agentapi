﻿using AgentAPI.Models;
using AgentAPI.Repositories;
using Newtonsoft.Json;
using System;
using System.Web.Mvc;
using static AgentAPI.Models.ResponseObjects;

namespace AgentAPI.Controllers
{
    public class BanksController : Controller
    {
        #region Get
        [AllowAnonymous]
        public JsonResult Get(RequestObjects.GetBanks p)
        {
            try
            {
                string res = ForwardDAL.PostToAPI(this.Request, "getbanks", JsonConvert.SerializeObject(p));

                try
                {
                    return Json(JsonConvert.DeserializeObject<Banks>(res), JsonRequestBehavior.DenyGet);
                }
                catch
                {
                    return Json(res, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(ex);
            }
        }
        #endregion
    }
}
