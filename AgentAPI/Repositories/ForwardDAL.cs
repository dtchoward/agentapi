﻿using System;
using System.Linq;
using System.Net;

namespace AgentAPI.Repositories
{
    public class ForwardDAL
    {
        public static string PostToAPI(System.Web.HttpRequestBase request, string endpoint, string json)
        {
            string nonce = "", sig = "", body = "", ip = "", companyID = "";
            GetHeaderValues(request, out nonce, out sig, out body, out ip, out companyID);

            HttpWebRequest req = WebRequest.Create(GetURL() + endpoint) as HttpWebRequest;
            req.Method = "POST";
            req.ContentType = "application/json";
            req.Headers["ContentType"] = "text/xml;charset=utf-8";
            req.Headers["nonce"] = nonce;
            req.Headers["signature"] = sig;
            req.Headers["ipSender"] = ip;
            req.Headers["companyID"] = companyID;

            byte[] byteData = System.Text.UTF8Encoding.UTF8.GetBytes(json);
            req.ContentLength = byteData.Length;
            using (System.IO.Stream postStream = req.GetRequestStream())
            {
                postStream.Write(byteData, 0, byteData.Length);
            }

            try
            {
                using (HttpWebResponse response = req.GetResponse() as HttpWebResponse)
                {
                    System.IO.StreamReader reader = new System.IO.StreamReader(response.GetResponseStream());
                    return reader.ReadToEnd();
                }
            }
            catch (WebException e)
            {
                return e.Message;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        private static string GetURL()
        {
            try
            {
                return System.Configuration.ConfigurationManager.AppSettings["url"].ToString();
            }
            catch
            {
                return "";
            }
        }

        public static void GetHeaderValues(System.Web.HttpRequestBase request, out string nonce, out string signature, out string body, 
            out string ip, out string companyID)
        {
            nonce = GetRequestHeaderValue(request, "nonce");
            signature = GetRequestHeaderValue(request, "signature");
            companyID = GetRequestHeaderValue(request, "companyID");
            ip = "";

            try // for load balancer
            {
                ip = (System.Web.HttpContext.Current.Request.Headers.GetValues("X-Forwarded-For")).FirstOrDefault();
            }
            catch {}
            
            if (string.IsNullOrEmpty(ip)) // if no load balancer, above should by empty, so take sender address
            {
                ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].Trim();
            }

            System.IO.StreamReader reader = new System.IO.StreamReader(request.InputStream);
            reader.BaseStream.Position = 0;
            body = reader.ReadToEnd();
        }

        private static string GetRequestHeaderValue(System.Web.HttpRequestBase request, string name)
        {
            try
            {
                return string.Join("", request.Headers[request.Headers.AllKeys.Where(x => x.ToUpper() == name.ToUpper()).FirstOrDefault()]);
            }
            catch
            {
                return "";
            }
        }
    }
}